This is a proof of concept web service for the Common Workflow Language.  It
works with any `cwl-runner` that supports the CWL standard command line interface:
http://www.commonwl.org/draft-3/CommandLineTool.html#Executing_CWL_documents_as_scripts

# Theory of operation:

* Accept job order via HTTP POST, create job and redirect to job URL
* Client can poll for job status
* Client can get streaming logs (stderr of `cwl-runner`)

# Installation:

```
python setup.py install
```

# Testing the application:

1. `cp instance/application.example.cfg instance/application.cfg`
2. `cp tests/resources/crypto_files/disposable.private.pem instance/private_key.pem`
3. `cp tests/resources/crypto_files/disposable.public.pem instance/public_cert.pem`
4. `tox`

To update dependencies, modify setup.py and then remove .tox directory.


# Run standalone server:

```
wes-server
```

# Run a job, get status, get log:

```
$ curl -L -d '{"message": "It works"}' http://localhost:5000/run?wf=https://raw.githubusercontent.com/common-workflow-language/common-workflow-language/master/draft-3/examples/1st-tool.cwl
{
    "state": "Running",
    "run": "https://raw.githubusercontent.com/common-workflow-language/common-workflow-language/master/draft-3/examples/1st-tool.cwl",
    "log": "http://localhost:5000/jobs/0/log",
    "input": {
        "message": "It works"
    },
    "output": null,
    "id": "http://localhost:5000/jobs/0"
}
```

```
$ curl http://localhost:5000/jobs/0
{
    "state": "Success",
    "run": "https://raw.githubusercontent.com/common-workflow-language/common-workflow-language/master/draft-3/examples/1st-tool.cwl",
    "log": "http://localhost:5000/jobs/0/log",
    "input": {
        "message": "It works"
    },
    "output": {},
    "id": "http://localhost:5000/jobs/0"
}
```

```
$ curl http://localhost:5000/jobs/0/log
cwl-runner 1.0.20160518201549
[job 1st-tool.cwl] /tmp/tmpKcoc_I$ echo \
    'It works'
It works
Final process status is success
```
